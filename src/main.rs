use anyhow::bail;
use clap::{App, Arg};
use git2::{Repository, RepositoryState, StatusOptions};
use regex::Regex;
use reqwest::StatusCode;
use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha256};
use std::{
    collections::{HashMap, HashSet},
    fs::{create_dir_all, File, OpenOptions},
    io::{BufRead, BufReader, Read, Write},
    path::{Path, PathBuf},
};

// struct definitions copied from https://github.com/rust-lang/crates.io/tree/master/src

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct Crate {
    /// The name of the package.
    ///
    /// This must only contain alphanumeric, `-`, or `_` characters.
    pub name: String,

    /// The version of the package this row is describing.
    ///
    /// This must be a valid version number according to the Semantic
    /// Versioning 2.0.0 spec at https://semver.org/.
    pub vers: String,

    /// Array of direct dependencies of the package.
    pub deps: Vec<Dependency>,

    /// A SHA256 checksum of the `.crate` file.
    pub cksum: String,

    /// Set of features defined for the package.
    ///
    /// Each feature maps to an array of features or dependencies it enables.
    pub features: HashMap<String, Vec<String>>,

    /// Boolean of whether or not this version has been yanked.
    pub yanked: Option<bool>,

    /// The `links` string value from the package's manifest, or null if not
    /// specified. This field is optional and defaults to null.
    #[serde(default)]
    pub links: Option<String>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct Dependency {
    /// Name of the dependency.
    ///
    /// If the dependency is renamed from the original package name,
    /// this is the new name. The original package name is stored in
    /// the `package` field.
    pub name: String,

    /// The semver requirement for this dependency.
    /// This must be a valid version requirement defined at
    /// https://github.com/steveklabnik/semver#requirements.
    pub req: String,

    /// Array of features (as strings) enabled for this dependency.
    pub features: Vec<String>,

    /// Boolean of whether or not this is an optional dependency.
    pub optional: bool,

    /// Boolean of whether or not default features are enabled.
    pub default_features: bool,

    /// The target platform for the dependency.
    ///
    /// null if not a target dependency.
    /// Otherwise, a string such as "cfg(windows)".
    pub target: Option<String>,

    /// The dependency kind.
    ///
    /// "dev", "build", or "normal".
    /// Note: this is a required field, but a small number of entries
    /// exist in the crates.io index with either a missing or null
    /// `kind` field due to implementation bugs.
    pub kind: Option<cargo_metadata::DependencyKind>,

    /// The URL of the index of the registry where this dependency is
    /// from as a string. If not specified or null, it is assumed the
    /// dependency is in the current registry.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub registry: Option<String>,

    /// If the dependency is renamed, this is a string of the actual
    /// package name. If not specified or null, this dependency is not
    /// renamed.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub package: Option<String>,
}

// #[derive(Copy, Clone, Serialize, Debug)]
// #[serde(rename_all = "lowercase")]
// #[repr(u32)]
// pub enum DependencyKind {
//     Normal = 0,
//     Build = 1,
//     Dev = 2,
// }

impl From<cargo_metadata::Package> for Crate {
    fn from(pkg: cargo_metadata::Package) -> Self {
        let deps = pkg.dependencies.into_iter().map(Dependency::from).collect();

        Crate {
            name: pkg.name,
            vers: format!("{}", pkg.version),
            deps,
            cksum: "TODO".to_string(),
            features: pkg.features,
            yanked: Some(false),
            links: pkg.links,
        }
    }
}

impl From<cargo_metadata::Dependency> for Dependency {
    fn from(dep: cargo_metadata::Dependency) -> Self {
        Dependency {
            name: dep.name,
            req: format!("{}", dep.req),
            features: dep.features,
            optional: dep.optional,
            default_features: dep.uses_default_features,
            target: dep.target.map(|t| format!("{}", t)),
            kind: Some(dep.kind),
            registry: Some(
                dep.registry
                    .unwrap_or("https://github.com/rust-lang/crates.io-index".to_string()),
            ),
            package: None,
        }
    }
}

fn hash_crate_file<P: AsRef<Path>>(p: P) -> std::io::Result<String> {
    let mut file = File::open(p)?;

    let mut buf = [0; 1024];
    let mut hasher = Sha256::new();

    loop {
        let num = file.read(&mut buf[..])?;
        if num == 0 {
            break;
        }
        hasher.update(&buf[..num]);
    }

    let hash = hasher.finalize();

    Ok(hex::encode(&hash[..]))
}

fn generate_index_path(pkg_name: &str) -> PathBuf {
    let pkg_name = pkg_name.to_lowercase();
    let root = match pkg_name.len() {
        0 => panic!(),
        1 => PathBuf::from("1"),
        2 => PathBuf::from("2"),
        3 => PathBuf::from("3").join(String::from(pkg_name.chars().next().unwrap())),
        _ => {
            let mut chars = pkg_name.chars();
            let a = chars.next().unwrap();
            let b = chars.next().unwrap();
            let c = chars.next().unwrap();
            let d = chars.next().unwrap();

            PathBuf::from(format!("{}{}", a, b)).join(format!("{}{}", c, d))
        }
    };

    root.join(pkg_name)
}

fn upload_to_gitlab(
    krate: &Crate,
    path_on_disk: &Path,
    base_url: &str,
    token: &str,
) -> anyhow::Result<()> {
    let crate_file = File::open(path_on_disk)?;

    let client = reqwest::blocking::Client::new();
    let resp = client
        .put(dbg!(format!(
            "{base_url}/packages/generic/{name}/{version}/{name}-{version}.crate",
            name = krate.name,
            version = krate.vers,
            base_url = base_url
        )))
        .basic_auth("cargo-update", Some(token))
        .body(crate_file)
        .send()?;

    if resp.status() == StatusCode::CREATED {
        return Ok(());
    }

    println!("{:?}", resp);
    Ok(())
}

/// Get metadata from a .crate file
fn get_metadata<P: AsRef<Path>>(crate_file: P) -> anyhow::Result<Crate> {
    let untar_dir = tempfile::tempdir()?;

    let crate_file = crate_file.as_ref();

    let file = File::open(crate_file)?;
    let gz_file = flate2::read::GzDecoder::new(file);
    let mut tar = tar::Archive::new(gz_file);

    let crate_name = crate_file.file_stem().unwrap();

    tar.unpack(&untar_dir)?;

    let crate_root = untar_dir.path().join(crate_name);
    let crate_manifest = crate_root.join("Cargo.toml");

    let mut cmd = cargo_metadata::MetadataCommand::new();
    cmd.current_dir(crate_root);
    cmd.manifest_path(crate_manifest);

    let md = cmd.exec().unwrap();

    let pkg = md.root_package().unwrap();
    let mut krate = Crate::from(pkg.clone());

    let crate_hash = hash_crate_file(crate_file).unwrap();
    krate.cksum = crate_hash;

    // println!("{:?}", krate);

    Ok(krate)
}

fn main() -> anyhow::Result<()> {
    let matches = App::new("cargo custom uploader")
        .about("")
        .arg(
            Arg::with_name("crate")
                .help("Path to the crate file to upload")
                .long_help(
                    "Path to the crate file to upload\n\
                    This is normally in the 'target/package' directory after running 'cargo package'",
                )
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("index_repo")
                .help("Path to local index repo")
                .long_help(
                    "Path to the local clone of the index repository.\n\
                    If not specified, this defaults to the current directory.",
                )
                .long("index")
                .takes_value(true)
                .default_value("."),
        )
        .arg(
            Arg::with_name("token")
                 .long("token")
                 .help("Gitlab upload token")
                 .long_help("The gitlab token to use when for authorization when uploading crates.\n\
                            If not specifed, the GITLAB_TOKEN environment variable is used.")
                 .takes_value(true)
        )
        .get_matches();

    // This overrides the project ID found in the config.json file of the repo
    let token_var = std::env::var("GITLAB_TOKEN").ok();
    let gitlab_token = matches
        .value_of("token")
        .map(ToString::to_string)
        .or(token_var)
        .expect("Missing gitlab token");

    let local_index_repo = matches.value_of("index_repo").unwrap();
    let config_json_str =
        std::fs::read_to_string(Path::new(local_index_repo).join("config.json")).unwrap();

    #[derive(Debug, Deserialize)]
    struct Config {
        dl: String,
    }

    let cfg: Config = serde_json::from_str(&config_json_str).unwrap();
    println!("{:?}", cfg);
    let r = Regex::new(r#"^(https://\S+/api/v4/projects/\d+)/packages/generic/"#).unwrap();
    let caps = r.captures(&cfg.dl).unwrap();

    let gitlab_base_url = caps.get(1).unwrap().as_str().to_string();
    println!("Using gitlab at: {}", gitlab_base_url);

    let local_crate_path = Path::new(matches.value_of("crate").unwrap());
    let krate = get_metadata(local_crate_path)?;

    // these restrictions are based on gitlab restrictions
    let crate_name_regex = Regex::new(r#"^[a-zA-Z0-9_.-]+$"#).unwrap();
    if !crate_name_regex.is_match(&krate.name) {
        bail!(
            "Crate name '{}' contains a character that gitlab does not support",
            krate.name
        );
    }

    // println!("{:#?}", pkg);

    let repo = Repository::open(local_index_repo)?;
    if repo.state() != RepositoryState::Clean {
        bail!("Local index repo is not in a clean state");
    }

    let status = repo.statuses(Some(StatusOptions::new().include_untracked(true)))?;
    if status.len() > 0 {
        eprintln!("The local repo is not in a clean state:");

        for idx in 0..status.len() {
            let status = status.get(idx).unwrap();
            println!("{:?}: {:?}", status.path(), status.status());
        }

        bail!("Local index repo is not in a clean state");
    }

    // println!("{}", js);
    // let path = generate_index_path(&pkg);
    // println!("{}", path.display());

    let local_registry_root = Path::new(matches.value_of("index_repo").unwrap());

    let index_json_list = generate_index_path(&krate.name);
    let index_file = local_registry_root.join(&index_json_list);
    if index_file.exists() {
        // make sure this version isn't already in the index
        for line in BufReader::new(File::open(&index_file)?).lines() {
            let c: Crate = serde_json::from_str(&line?)?;
            if c.vers == krate.vers {
                bail!(
                    "Version {} of {} is already in the index",
                    krate.vers,
                    krate.name
                );
            }
        }
    }

    upload_to_gitlab(&krate, local_crate_path, &gitlab_base_url, &gitlab_token)?;

    println!("Upload to gitlab successful");

    let _ = create_dir_all(index_file.parent().unwrap());

    let mut file = OpenOptions::new()
        .read(false)
        .write(true)
        .create(true)
        .append(true)
        .open(index_file)?;

    let js = serde_json::to_string(&krate)?;

    writeln!(file, "{}", js)?;

    let mut idx = repo.index().unwrap();
    idx.add_path(&index_json_list).unwrap();
    let new_tree_oid = idx.write_tree().unwrap();
    let new_tree = repo.find_tree(new_tree_oid).unwrap();

    let current_head = repo.head().unwrap().peel_to_commit().unwrap();

    let sig = repo.signature().unwrap();
    let new_commit = repo
        .commit(
            Some("HEAD"),
            &sig,
            &sig,
            &format!("Add {} {}", krate.name, krate.vers),
            &new_tree,
            &[&current_head],
        )
        .unwrap();
    repo.checkout_head(None).unwrap();

    println!("Update is committed: {}", new_commit);

    Ok(())
}

#[test]
fn foo() {
    get_metadata(r#"Z:\temp\04\another-test-lib\target\package\another-test-lib-0.1.0.crate"#)
        .unwrap();
}

#[test]
fn test() {
    // test against the crates.io repo

    let untar_dir = tempfile::tempdir().unwrap();
    println!("local tempdir: {}", untar_dir.path().display());

    fn download_crate(name: &str, version: &str, untar_dir: &Path) -> PathBuf {
        let mut resp = reqwest::blocking::get(format!(
            "https://crates.io/api/v1/crates/{name}/{version}/download",
            name = name,
            version = version
        ))
        .unwrap();

        let download_file = untar_dir.join(format!("{}-{}.crate", name, version));

        let mut file = File::create(&download_file).unwrap();
        resp.copy_to(&mut file).unwrap();

        download_file
    }

    // let downloaded_crate = download_crate("procfs", "0.9.0", &untar_dir);
    // let md = get_metadata(&downloaded_crate).unwrap();

    let crates_io_index = Path::new(r#"z:\devel\crates.io-index"#);

    let sub = generate_index_path("terminal_size");
    let crates_io_info = File::open(crates_io_index.join(sub)).unwrap();

    for line in BufReader::new(crates_io_info).lines() {
        let line = line.unwrap();
        let official_crate: Crate = serde_json::from_str(&line).unwrap();
        println!("Testing {} {}...", official_crate.name, official_crate.vers);

        // println!("{:?}", official_crate);

        let downloaded_file =
            download_crate(&official_crate.name, &official_crate.vers, untar_dir.path());
        let my_crate = get_metadata(&downloaded_file).unwrap();
        // println!("{:?}", my_crate);

        assert_eq!(official_crate.name, my_crate.name);
        assert_eq!(official_crate.deps.len(), my_crate.deps.len());
        assert_eq!(official_crate.features, my_crate.features);
        assert_eq!(official_crate.cksum, my_crate.cksum);
        // assert_eq!(official_crate.yanked, my_crate.yanked);
        assert_eq!(official_crate.links, my_crate.links);

        // for dep in &official_crate.deps {
        //     println!("{:?}", dep);
        // }

        fn vec_to_map(v: Vec<Dependency>) -> HashMap<String, Dependency> {
            let mut map = HashMap::new();
            for d in v {
                map.insert(d.name.clone(), d);
            }
            map
        }

        let official_deps = vec_to_map(official_crate.deps);
        let mut my_deps = vec_to_map(my_crate.deps);

        assert_eq!(
            official_deps.keys().collect::<HashSet<_>>(),
            my_deps.keys().collect::<HashSet<_>>()
        );

        for (dep_name, official_dep) in official_deps {
            let my_dep = my_deps.remove(&dep_name).unwrap();

            assert_eq!(official_dep.name, my_dep.name);
            assert_eq!(official_dep.features, my_dep.features);
            assert_eq!(
                semver::VersionReq::parse(&official_dep.req).unwrap(),
                semver::VersionReq::parse(&my_dep.req).unwrap()
            );
        }
    }
}
